import { Component, OnInit } from '@angular/core';
import { MedicineService } from '../shared/medicine/medicine.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-medicine-list',
  templateUrl: './medicine-list.component.html',
  styleUrls: ['./medicine-list.component.css']
})
export class MedicineListComponent implements OnInit {
  medicines: Array<any>;

  constructor(private medicineService: MedicineService) { }

  ngOnInit() {
    this.medicineService.getAll().subscribe(data => {
      this.medicines = data;
    });
  }
}
