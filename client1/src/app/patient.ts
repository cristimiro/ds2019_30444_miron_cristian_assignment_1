import { MedicationPlan } from './medicationPlan';

export class Patient{
  id: number;
  username: String;
  password: String;
  name: String;
  address: String;
  birth_date: Date;
  gender: String;
  medicationPlanSet: any[];
}