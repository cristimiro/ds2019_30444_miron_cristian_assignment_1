import { Component, OnInit } from '@angular/core';
import { CaregiverService } from '../shared/caregiver/caregiver.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Caregiver } from '../caregiver';

@Component({
  selector: 'app-caregiver-patients',
  templateUrl: './caregiver-patients.component.html',
  styleUrls: ['./caregiver-patients.component.css']
})
export class CaregiverPatientsComponent implements OnInit {

  caregiver: Caregiver;
  patients: any[] = [];
  sub: Subscription;

  constructor(private caregiverService: CaregiverService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params.id;
      if(id){
        this.caregiverService.get(id).subscribe((data: Caregiver) => {
          this.caregiver = data;
          this.patients = this.caregiver.patientSet;
          console.log(this.caregiver);
        });
      }
    });
  }

}
