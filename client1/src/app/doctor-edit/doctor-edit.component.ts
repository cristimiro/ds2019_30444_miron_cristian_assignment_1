import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { DoctorService } from '../shared/doctor/doctor.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-doctor-edit',
  templateUrl: './doctor-edit.component.html',
  styleUrls: ['./doctor-edit.component.css']
})
export class DoctorEditComponent implements OnInit, OnDestroy  {
  doctor: any = {};
  update: boolean = false;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private doctorService: DoctorService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params.id;
      if (id) {
        this.update = true;
        this.doctorService.get(id).subscribe((doctor: any) => {
          if (doctor) {
            this.doctor = doctor;
          } else {
            this.update = false;
            this.gotoList();
          }
        });
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  gotoList() {
    this.router.navigate(['/doctor-list']);
  }

  save(form: NgForm, update: any) {
    this.doctorService.save(form, update).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

  remove(id: string) {
    this.doctorService.remove(id).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }
}