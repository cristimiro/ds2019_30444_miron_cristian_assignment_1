import { Medicine } from './medicine';

export class MedicationPlan{
  id: Number;
  period: String;
  medicationSet: Medicine[];
}