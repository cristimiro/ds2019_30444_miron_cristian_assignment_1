import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DoctorService } from '../shared/doctor/doctor.service';

@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.css']
})
export class DoctorListComponent implements OnInit {

  doctors: Array<any>;

  constructor(private doctorService: DoctorService) { }

  ngOnInit() {
    this.doctorService.getAll().subscribe(data => {
      this.doctors = data;
    });
  }

}
