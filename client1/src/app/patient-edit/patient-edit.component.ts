import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { PatientService } from '../shared/patient/patient.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-patient-edit',
  templateUrl: './patient-edit.component.html',
  styleUrls: ['./patient-edit.component.css']
})
export class PatientEditComponent implements OnInit, OnDestroy {
  patient: any = {};
  update: boolean = false;
  userAccess: boolean = false;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private patientService: PatientService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params.id;
      const user = JSON.parse(localStorage.getItem("user"));
        if (id) {
          this.update = true;
          this.patientService.get(id).subscribe((patient: any) => {
            if (patient) {
              this.patient = patient;
            } else {
              this.update = false;
              this.gotoList();
            }
          });
        }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  gotoList() {
    this.router.navigate(['/patient-list']);
  }

  save(form: NgForm, update: any) {
    this.patientService.save(form, update).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

  remove(id: string) {
    this.patientService.remove(id).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }
}