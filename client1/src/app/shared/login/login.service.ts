import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public API = '//localhost:8080/login';

  constructor(private http: HttpClient) { }

  getUser(username: String){
    return this.http.get(this.API + '/' + username)
  }
}
