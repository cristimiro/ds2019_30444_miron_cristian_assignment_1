import { TestBed } from '@angular/core/testing';

import { MedicationPlanService } from './medication-plan.service';

describe('MedicationPlanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MedicationPlanService = TestBed.get(MedicationPlanService);
    expect(service).toBeTruthy();
  });
});
