import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MedicationPlanService {
  public API = '//localhost:8080/medication-plan';
  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(this.API);
  }

  save(period: String, m_id: String){
    var hsh = {period: null, m_id: null};
    hsh.period = period;
    hsh.m_id = m_id;
    return this.http.post(this.API + '/add', hsh );
  }

  get(id: string) {
    return this.http.get(this.API + '/' + id);
  }

  remove(id: string) {
    return this.http.delete(this.API + '/' + id);
  }
}
