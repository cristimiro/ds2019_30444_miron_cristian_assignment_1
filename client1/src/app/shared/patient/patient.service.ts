import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  public API = '//localhost:8080/patients';

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(this.API);
  }

  save(patient: any, update: boolean): Observable<any> {
    let result: Observable<any>;
    if (update) {
      result = this.http.put(this.API, patient);
    } else {
      result = this.http.post(this.API, patient);
    }
    return result;
  }

  get(id: string) {
    return this.http.get(this.API + '/' + id);
  }

  remove(id: string) {
    return this.http.delete(this.API + '/' + id);
  }

  saveLink(medPlan_id: String, patient_id: String){
    var parameters = {medicationPlan: null,patient: null};
    parameters.medicationPlan = medPlan_id;
    parameters.patient = patient_id;
    return this.http.post(this.API + "/add-plan",parameters);
  }
}
