import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MedicineService {
  public API = '//localhost:8080/medication';

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(this.API);
  }

  save(caregiver: any, update: boolean): Observable<any> {
    let result: Observable<any>;
    if (update) {
      result = this.http.put(this.API, caregiver);
    } else {
      result = this.http.post(this.API, caregiver);
    }
    return result;
  }

  get(id: string) {
    return this.http.get(this.API + '/' + id);
  }

  remove(id: string) {
    return this.http.delete(this.API + '/' + id);
  }
  
}
