import { Component, OnInit } from '@angular/core';
import {  FormGroup,FormControl } from '@angular/forms';
import { LoginService } from '../shared/login/login.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})

export class LoginPageComponent implements OnInit {

  user: any = {};

  username: String
  password: String
  loginForm: FormGroup

  constructor(private loginService: LoginService,
    private route: ActivatedRoute,private router: Router) { }

  ngOnInit() {
    this.loginForm = new FormGroup({
      username: new FormControl(''),
      password: new FormControl(''),
    });
  }

  login(){
    this.loginService.getUser(this.loginForm.value.username).subscribe(data => {
      this.user = data;
      // localStorage.setItem("userRole", JSON.stringify(data));
      localStorage.setItem("user", JSON.stringify(data));
      const user = JSON.parse(localStorage.getItem("user"));
      console.log(user.role);
      if(user.role === "patient"){
        this.gotoPatient(user.id);
      } else if (user.role === "doctor") {
        this.gotoDoctor(user.id);
      } else {
        this.gotoCaregiver(user.id);
      }
      });
  }

  gotoPatient(id: string) {
    this.router.navigate(['/patient-show',id]);
  }

  gotoDoctor(id: string) {
    this.router.navigate(['/patient-list']);
  }

  gotoCaregiver(id: string) {
    this.router.navigate(['/caregiver-show',id]);
  }
}
