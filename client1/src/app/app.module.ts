import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule,MatGridListModule, MatIconModule, MatCardModule, MatInputModule, MatListModule, MatToolbarModule, MatSidenavModule, MatDialogModule } from '@angular/material';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';


import { HttpClientModule } from '@angular/common/http';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientEditComponent } from './patient-edit/patient-edit.component';
import { PatientShowComponent } from './patient-show/patient-show.component';
import { HomePageComponent } from './home-page/home-page.component';
import { CaregiverListComponent } from './caregiver-list/caregiver-list.component';
import { CaregiverEditComponent } from './caregiver-edit/caregiver-edit.component';
import { CaregiverShowComponent } from './caregiver-show/caregiver-show.component';
import { MedicineListComponent } from './medicine-list/medicine-list.component';
import { MedicineEditComponent } from './medicine-edit/medicine-edit.component';
import { MedicineShowComponent } from './medicine-show/medicine-show.component';
import { DoctorEditComponent } from './doctor-edit/doctor-edit.component';
import { DoctorShowComponent } from './doctor-show/doctor-show.component';
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { CaregiverPatientsComponent } from './caregiver-patients/caregiver-patients.component';
import { MedicationPlanEditComponent } from './medication-plan-edit/medication-plan-edit.component';
import { MedicationPlanListComponent } from './medication-plan-list/medication-plan-list.component';
import { PatientCaregiverAddComponent } from './patient-caregiver-add/patient-caregiver-add.component';
import { PatientMedicationAddComponent } from './patient-medication-add/patient-medication-add.component';

@NgModule({
  declarations: [
    AppComponent,
    PatientListComponent,
    PatientEditComponent,
    PatientShowComponent,
    HomePageComponent,
    CaregiverListComponent,
    CaregiverEditComponent,
    CaregiverShowComponent,
    MedicineListComponent,
    MedicineEditComponent,
    MedicineShowComponent,
    DoctorEditComponent,
    DoctorShowComponent,
    DoctorListComponent,
    LoginPageComponent,
    CaregiverPatientsComponent,
    MedicationPlanEditComponent,
    MedicationPlanListComponent,
    PatientCaregiverAddComponent,
    PatientMedicationAddComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatButtonModule,
    MatDialogModule,
    MatCardModule,
    MatInputModule,
    MatListModule,
    MatToolbarModule,
    MatSidenavModule,
    FormsModule,
    ReactiveFormsModule ,
    MatGridListModule,
    MatIconModule
  ],
  exports: [
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
