import { Component, OnInit } from '@angular/core';
import { PatientService } from '../shared/patient/patient.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {
  patients: Array<any>;

  constructor(private patientService: PatientService) { }

  ngOnInit() {
    this.patientService.getAll().subscribe(data => {
      this.patients = data;
    });
  }
  
}
