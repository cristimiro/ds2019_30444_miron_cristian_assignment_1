import { Component, OnInit } from '@angular/core';
import { MedicationPlanService } from '../shared/medicationPlan/medication-plan.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-medication-plan-list',
  templateUrl: './medication-plan-list.component.html',
  styleUrls: ['./medication-plan-list.component.css']
})
export class MedicationPlanListComponent implements OnInit {

  medicationPlans: Array<any>;

  constructor(private medicationPlanService: MedicationPlanService) { }

  ngOnInit() {
    this.medicationPlanService.getAll().subscribe(data => {
      this.medicationPlans = data;
    });
  }

}
