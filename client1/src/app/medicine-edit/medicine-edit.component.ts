import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { MedicineService } from '../shared/medicine/medicine.service';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-medicine-edit',
  templateUrl: './medicine-edit.component.html',
  styleUrls: ['./medicine-edit.component.css']
})
export class MedicineEditComponent implements OnInit {
  medicine: any = {};
  update: boolean = false;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private medicineService: MedicineService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params.id;
      if (id) {
        this.update = true;
        this.medicineService.get(id).subscribe((medicine: any) => {
          if (medicine) {
            this.medicine = medicine;
          } else {
            this.update = false;
            this.gotoList();
          }
        });
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  gotoList() {
    this.router.navigate(['/medicine-list']);
  }

  save(form: NgForm, update: any) {
    this.medicineService.save(form, update).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

  remove(id: string) {
    this.medicineService.remove(id).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }
}
