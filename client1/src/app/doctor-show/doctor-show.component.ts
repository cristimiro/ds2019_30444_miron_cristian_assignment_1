import { Component, OnInit } from '@angular/core';
import { DoctorService } from '../shared/doctor/doctor.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-doctor-show',
  templateUrl: './doctor-show.component.html',
  styleUrls: ['./doctor-show.component.css']
})
export class DoctorShowComponent implements OnInit {

  doctor: any = {};
  update: boolean = false;
  sub: Subscription;

  constructor(private doctorService: DoctorService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params.id;
      if (id) {
        this.doctorService.get(id).subscribe((doctor: any) => {
          if (doctor) {
            this.doctor = doctor;
          }
        });
      }
    });
  }

  gotoList() {
    this.router.navigate(['/doctor-list']);
  }

  remove(id: string) {
    this.doctorService.remove(id).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

}
