export class Caregiver{
  id: number;
  username: String;
  password: String;
  name: String;
  address: String;
  birth_date: Date;
  gender: String;
  patientSet: any[];
}