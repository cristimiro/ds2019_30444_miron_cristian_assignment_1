export class Medicine{
  id: number;
  dosage: string;
  name: string;
}