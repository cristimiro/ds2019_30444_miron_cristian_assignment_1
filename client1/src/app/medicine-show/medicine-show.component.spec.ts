import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicineShowComponent } from './medicine-show.component';

describe('MedicineShowComponent', () => {
  let component: MedicineShowComponent;
  let fixture: ComponentFixture<MedicineShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicineShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicineShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
