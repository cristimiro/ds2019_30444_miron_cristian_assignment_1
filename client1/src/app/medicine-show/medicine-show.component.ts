import { Component, OnInit } from '@angular/core';
import { MedicineService } from '../shared/medicine/medicine.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-medicine-show',
  templateUrl: './medicine-show.component.html',
  styleUrls: ['./medicine-show.component.css']
})
export class MedicineShowComponent implements OnInit {

  medicine: any = {};
  update: boolean = false;
  sub: Subscription;

  constructor(private medicineService: MedicineService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params.id;
      if (id) {
        this.medicineService.get(id).subscribe((medicine: any) => {
          if (medicine) {
            this.medicine = medicine;
          }
        });
      }
    });
  }

  gotoList() {
    this.router.navigate(['/medicine-list']);
  }

  remove(id: string) {
    this.medicineService.remove(id).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

}
