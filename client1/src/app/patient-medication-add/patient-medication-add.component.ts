import { Component, OnInit } from '@angular/core';
import { PatientService } from '../shared/patient/patient.service';
import { MedicationPlanService } from '../shared/medicationPlan/medication-plan.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Patient } from '../patient';
import { MedicationPlan } from '../medicationPlan';

import { NgForm, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-patient-medication-add',
  templateUrl: './patient-medication-add.component.html',
  styleUrls: ['./patient-medication-add.component.css']
})
export class PatientMedicationAddComponent implements OnInit {

  patients: Array<Patient>;
  medicationPlans: Array<MedicationPlan>;

  medPlan_id: String
  patient_id: String
  mapForm: FormGroup

  constructor(private route: ActivatedRoute,
    private router: Router,
    private medicationPlanService: MedicationPlanService,
    private patientService: PatientService) { }

  ngOnInit() {
    this.patientService.getAll().subscribe(data => {
      this.patients = data;
    });
    this.medicationPlanService.getAll().subscribe(data => {
      this.medicationPlans = data;
    });
    this.mapForm = new FormGroup({
      medPlan_id: new FormControl(''),
      patient_id: new FormControl(''),
    });
  }

  save(){
    this.patientService.saveLink(this.mapForm.value.medPlan_id,this.mapForm.value.patient_id).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

  gotoList() {
    this.router.navigate(['/patient-list']);
  }

}
