import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientMedicationAddComponent } from './patient-medication-add.component';

describe('PatientMedicationAddComponent', () => {
  let component: PatientMedicationAddComponent;
  let fixture: ComponentFixture<PatientMedicationAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientMedicationAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientMedicationAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
