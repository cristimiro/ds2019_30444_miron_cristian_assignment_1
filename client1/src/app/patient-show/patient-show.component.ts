import { Component, OnInit } from '@angular/core';
import { PatientService } from '../shared/patient/patient.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Patient } from '../patient';
import { MedicationPlan } from '../medicationPlan';


@Component({
  selector: 'app-patient-show',
  templateUrl: './patient-show.component.html',
  styleUrls: ['./patient-show.component.css']
})
export class PatientShowComponent implements OnInit {

  patient: Patient;
  medicationPlans: MedicationPlan[] = [];
  update: boolean = false;
  sub: Subscription;

  constructor(private patientService: PatientService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params.id;
      if (id) {
        this.patientService.get(id).subscribe((data: Patient) => {
          if (data) {
            // localStorage.setItem("patient", JSON.stringify(data));
            console.log(data);
            this.patient = data;
            console.log(this.patient.id);
            console.log(this.patient.medicationPlanSet)
            // const patient = JSON.parse(localStorage.getItem("patient"))
            this.medicationPlans = this.patient.medicationPlanSet;
          }
        });
      }
    });
  }

  gotoList() {
    this.router.navigate(['/patient-list']);
  }

  remove(id: string) {
    this.patientService.remove(id).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

}
