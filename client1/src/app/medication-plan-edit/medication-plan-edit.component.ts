import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { MedicationPlanService } from '../shared/medicationPlan/medication-plan.service';
import { NgForm, FormGroup, FormControl } from '@angular/forms';
import { MedicineService } from '../shared/medicine/medicine.service';
import { Medicine } from '../medicine';
import { MedicationPlan } from '../medicationPlan';

@Component({
  selector: 'app-medication-plan-edit',
  templateUrl: './medication-plan-edit.component.html',
  styleUrls: ['./medication-plan-edit.component.css']
})
export class MedicationPlanEditComponent implements OnInit {

  medicines: Medicine[] = [];
  medicationPlan: MedicationPlan;
  update: boolean = false;
  sub: Subscription;

  m_id: String
  medForm: FormGroup
  period: String

  constructor(private route: ActivatedRoute,
    private router: Router,
    private medicationPlanService: MedicationPlanService,
    private medicineService: MedicineService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params.id;
      if (id) {
        this.update = true;
        this.medicationPlanService.get(id).subscribe((medicationPlan: any) => {
          console.log(medicationPlan);
          if (medicationPlan) {
            this.medicationPlan = medicationPlan;
            this.medicines = this.medicationPlan.medicationSet;
            console.log(this.medicines);
          } else {
            this.update = false;
            this.gotoList();
          }
        });
      }else{
        this.medicineService.getAll().subscribe((data: Medicine[]) => {
          this.medicines = data;
        });
        this.medForm = new FormGroup({
          m_id: new FormControl(''),
          period: new FormControl(''),
        });
      }
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  gotoList() {
    this.router.navigate(['/medication-plan-list']);
  }

  save() {
    
    this.medicationPlanService.save(this.medForm.value.period,this.medForm.value.m_id).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

  remove(id: string) {
    this.medicationPlanService.remove(id).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

}
