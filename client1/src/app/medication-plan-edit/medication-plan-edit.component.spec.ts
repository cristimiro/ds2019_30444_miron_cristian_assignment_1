import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicationPlanEditComponent } from './medication-plan-edit.component';

describe('MedicationPlanEditComponent', () => {
  let component: MedicationPlanEditComponent;
  let fixture: ComponentFixture<MedicationPlanEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicationPlanEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicationPlanEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
