import { Component, OnInit } from '@angular/core';
import { CaregiverService } from '../shared/caregiver/caregiver.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-caregiver-list',
  templateUrl: './caregiver-list.component.html',
  styleUrls: ['./caregiver-list.component.css']
})
export class CaregiverListComponent implements OnInit{

  caregivers: Array<any>;

  constructor(private caregiverService: CaregiverService) { }

  ngOnInit() {
    this.caregiverService.getAll().subscribe(data => {
      this.caregivers = data;
    });
  }
}
