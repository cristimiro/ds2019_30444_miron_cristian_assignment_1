import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientCaregiverAddComponent } from './patient-caregiver-add.component';

describe('PatientCaregiverAddComponent', () => {
  let component: PatientCaregiverAddComponent;
  let fixture: ComponentFixture<PatientCaregiverAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientCaregiverAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientCaregiverAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
