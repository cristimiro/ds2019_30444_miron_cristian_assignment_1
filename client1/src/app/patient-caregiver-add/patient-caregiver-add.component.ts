import { Component, OnInit } from '@angular/core';
import { PatientService } from '../shared/patient/patient.service';
import { CaregiverService } from '../shared/caregiver/caregiver.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Patient } from '../patient';
import { Caregiver } from '../caregiver';

import { NgForm, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-patient-caregiver-add',
  templateUrl: './patient-caregiver-add.component.html',
  styleUrls: ['./patient-caregiver-add.component.css']
})
export class PatientCaregiverAddComponent implements OnInit {

  patients: Array<Patient>;
  caregivers: Array<Caregiver>;

  c_id: String
  p_id: String
  mapForm: FormGroup

  constructor(private route: ActivatedRoute,
    private router: Router,private caregiverService: CaregiverService,private patientService: PatientService) { }

  ngOnInit() {
    this.patientService.getAll().subscribe(data => {
      this.patients = data;
    });
    this.caregiverService.getAll().subscribe(data => {
      this.caregivers = data;
    });
    this.mapForm = new FormGroup({
      c_id: new FormControl(''),
      p_id: new FormControl(''),
    });
  }

  save(){
    this.caregiverService.saveLink(this.mapForm.value.c_id,this.mapForm.value.p_id).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

  gotoList() {
    this.router.navigate(['/caregiver-list']);
  }

}
