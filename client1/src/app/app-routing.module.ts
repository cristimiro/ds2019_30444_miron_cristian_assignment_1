import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientEditComponent } from './patient-edit/patient-edit.component';
import { PatientShowComponent } from './patient-show/patient-show.component';
import { CaregiverListComponent } from './caregiver-list/caregiver-list.component';
import { CaregiverEditComponent } from './caregiver-edit/caregiver-edit.component';
import { CaregiverShowComponent } from './caregiver-show/caregiver-show.component';
import { HomePageComponent } from './home-page/home-page.component';
import { MedicineListComponent } from './medicine-list/medicine-list.component';
import { MedicineEditComponent } from './medicine-edit/medicine-edit.component';
import { MedicineShowComponent } from './medicine-show/medicine-show.component';
import { DoctorListComponent } from './doctor-list/doctor-list.component';
import { DoctorEditComponent } from './doctor-edit/doctor-edit.component';
import { DoctorShowComponent } from './doctor-show/doctor-show.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { CaregiverPatientsComponent } from './caregiver-patients/caregiver-patients.component';
import { MedicationPlanListComponent } from './medication-plan-list/medication-plan-list.component';
import { MedicationPlanEditComponent } from './medication-plan-edit/medication-plan-edit.component';
import { PatientCaregiverAddComponent } from './patient-caregiver-add/patient-caregiver-add.component';
import { PatientMedicationAddComponent } from './patient-medication-add/patient-medication-add.component';

let routes: Routes = [
  { path: '',
    component: LoginPageComponent,
  },
  {
    path: 'patient-list',
    component: PatientListComponent,
  },
  {
    path: 'patient-add',
    component: PatientEditComponent,
  },
  {
    path: 'patient-edit/:id',
    component: PatientEditComponent
  },
  {
    path: 'patient-show/:id',
    component: PatientShowComponent
  }
  ,
  {
    path: 'caregiver-list',
    component: CaregiverListComponent,
  },
  {
    path: 'caregiver-add',
    component: CaregiverEditComponent,
  },
  {
    path: 'caregiver-show/:id',
    component: CaregiverShowComponent,
  },
  {
    path: 'caregiver-edit/:id',
    component: CaregiverEditComponent,
  },
  {
    path: 'caregiver-patients/:id',
    component: CaregiverPatientsComponent,
  }
  ,
  {
    path: 'medicine-list',
    component: MedicineListComponent,
  },
  {
    path: 'medicine-add',
    component: MedicineEditComponent,
  },
  {
    path: 'medicine-show/:id',
    component: MedicineShowComponent,
  },
  {
    path: 'medicine-edit/:id',
    component: MedicineEditComponent,
  }
  ,
  {
    path: 'doctor-list',
    component: DoctorListComponent,
  },
  {
    path: 'doctor-add',
    component: DoctorEditComponent,
  },
  {
    path: 'doctor-show/:id',
    component:DoctorShowComponent,
  },
  {
    path: 'doctor-edit/:id',
    component: DoctorEditComponent,
  },
  {
    path: 'medication-plan-list',
    component: MedicationPlanListComponent,
  },
  {
    path: 'medication-plan-add',
    component: MedicationPlanEditComponent,
  },
  {
    path: 'medication-plan-edit/:id',
    component: MedicationPlanEditComponent,
  }
  ,
  {
    path: 'login',
    component: LoginPageComponent,
  },
  {
    path: 'home-page',
    component: HomePageComponent,
  }
  ,
  {
    path: 'patient-caregiver',
    component: PatientCaregiverAddComponent,
  }
  ,
  {
    path: 'patient-plan',
    component: PatientMedicationAddComponent,
  }
];

function setRoutes() {
  const user = JSON.parse(localStorage.getItem("user"));
  console.log(user);
  if (user.role === "doctor") {
    routes = [{
      path: 'doctor-list',
      component: DoctorListComponent,
    },
    {
      path: 'doctor-add',
      component: DoctorEditComponent,
    },
    {
      path: 'doctor-show/:id',
      component:DoctorShowComponent,
    },
    {
      path: 'doctor-edit/:id',
      component: DoctorEditComponent,
    },
    {
      path: 'patient-list',
      component: PatientListComponent,
    },
    {
      path: 'patient-add',
      component: PatientEditComponent,
    },
    {
      path: 'patient-edit/:id',
      component: PatientEditComponent
    },
    {
      path: 'patient-show/:id',
      component: PatientShowComponent
    },
    {
      path: 'caregiver-list',
      component: CaregiverListComponent,
    },
    {
      path: 'caregiver-add',
      component: CaregiverEditComponent,
    },
    {
      path: 'caregiver-show/:id',
      component: CaregiverShowComponent,
    },
    {
      path: 'caregiver-edit/:id',
      component: CaregiverEditComponent,
    },
    {
      path: 'caregiver-patients/:id',
      component: CaregiverPatientsComponent,
    },
    {
      path: 'medicine-list',
      component: MedicineListComponent,
    },
    {
      path: 'medicine-add',
      component: MedicineEditComponent,
    },
    {
      path: 'medicine-show/:id',
      component: MedicineShowComponent,
    },
    {
      path: 'medicine-edit/:id',
      component: MedicineEditComponent,
    },
    {
      path: 'medication-plan-list',
      component: MedicationPlanListComponent,
    },
    {
      path: 'medication-plan-add',
      component: MedicationPlanEditComponent,
    },
    {
      path: 'medication-plan-edit/:id',
      component: MedicationPlanEditComponent,
    }
    ,
    {
      path: 'login',
      component: LoginPageComponent,
    },
    {
      path: 'home-page',
      component: HomePageComponent,
    },
    {
      path: 'patient-caregiver',
      component: PatientCaregiverAddComponent,
    }
    ,
    {
      path: 'patient-plan',
      component: PatientMedicationAddComponent,
    }]
  } else if (user.role === "caregiver"){
    routes = [
      {
        path: 'caregiver-show/:id',
        component: CaregiverShowComponent,
      },
      {
        path: 'caregiver-patients/:id',
        component: CaregiverPatientsComponent,
      },
      {
        path: 'login',
        component: LoginPageComponent,
      },
      {
        path: 'home-page',
        component: HomePageComponent,
      }
    ]
  } else {
    routes = [{
      path: 'patient-show/:id',
      component: PatientShowComponent
    },
    {
      path: 'login',
      component: LoginPageComponent,
    },
    {
      path: 'home-page',
      component: HomePageComponent,
    }]
  }
  console.log(routes);
}



setRoutes();

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
