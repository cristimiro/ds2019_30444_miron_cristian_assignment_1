import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { CaregiverService } from '../shared/caregiver/caregiver.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-caregiver-edit',
  templateUrl: './caregiver-edit.component.html',
  styleUrls: ['./caregiver-edit.component.css']
})
export class CaregiverEditComponent implements OnInit, OnDestroy {

  caregiver: any = {};
  update: boolean = false;
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private caregiverService: CaregiverService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params.id;
      if (id) {
        this.update = true;
        this.caregiverService.get(id).subscribe((caregiver: any) => {
          if (caregiver) {
            this.caregiver = caregiver;
          } else {
            this.update = false;
            this.gotoList();
          }
        });
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  gotoList() {
    this.router.navigate(['/caregiver-list']);
  }

  save(form: NgForm, update: any) {
    this.caregiverService.save(form, update).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

  remove(id: string) {
    this.caregiverService.remove(id).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }
}
