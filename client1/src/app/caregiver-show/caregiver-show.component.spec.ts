import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaregiverShowComponent } from './caregiver-show.component';

describe('CaregiverShowComponent', () => {
  let component: CaregiverShowComponent;
  let fixture: ComponentFixture<CaregiverShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaregiverShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaregiverShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
