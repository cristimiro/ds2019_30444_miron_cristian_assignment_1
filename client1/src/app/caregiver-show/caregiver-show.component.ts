import { Component, OnInit } from '@angular/core';
import { CaregiverService } from '../shared/caregiver/caregiver.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-caregiver-show',
  templateUrl: './caregiver-show.component.html',
  styleUrls: ['./caregiver-show.component.css']
})
export class CaregiverShowComponent implements OnInit {

  caregiver: any = {};
  update: boolean = false;
  sub: Subscription;

  constructor(private caregiverService: CaregiverService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params.id;
      if (id) {
        this.caregiverService.get(id).subscribe((caregiver: any) => {
          if (caregiver) {
            this.caregiver = caregiver;
            
          }
        });
      }
    });
  }

  goToPatients(id: string){
    this.router.navigate(['/caregiver-patients/',id]);
  }

  gotoList() {
    this.router.navigate(['/caregiver-list']);
  }

  remove(id: string) {
    this.caregiverService.remove(id).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

}
