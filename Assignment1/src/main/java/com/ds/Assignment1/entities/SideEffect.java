package com.ds.Assignment1.entities;

import lombok.Data;

import javax.persistence.*;

import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Data

@Entity
public class SideEffect {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "sideEffect")
    private String sideEffect;

    @ManyToMany(mappedBy = "sideEffectSet")
    private Set<Medication> medicationSet;

    public SideEffect(Integer id, String sideEffect) {
        this.id = id;
        this.sideEffect = sideEffect;
    }

    public SideEffect(String sideEffect) {
        this.sideEffect = sideEffect;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSideEffect() {
        return sideEffect;
    }

    public void setSideEffect(String sideEffect) {
        this.sideEffect = sideEffect;
    }

    public Set<Medication> getMedicationSet() {
        return medicationSet;
    }

    public void setMedicationSet(Set<Medication> medicationSet) {
        this.medicationSet = medicationSet;
    }
}
