package com.ds.Assignment1.entities;

import lombok.Data;

import javax.persistence.*;

import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication")
public class Medication {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name="dosage")
    private String dosage;

    @ManyToMany
    @JoinTable(
            name = "medication_sideEffect",
            joinColumns = @JoinColumn(name = "medication_id"),
            inverseJoinColumns = @JoinColumn(name = "sideEffect_id"))
    private Set<SideEffect> sideEffectSet;

    @ManyToMany(mappedBy = "medicationSet", fetch = FetchType.EAGER)
    private Set<MedicationPlan> medicationPlanSet;

    public Medication(Integer id,String name, String dosage, Set<SideEffect> sideEffectSet) {
        this.id = id;
        this.name = name;
        this.dosage = dosage;
        this.sideEffectSet = sideEffectSet;
    }

    public Medication(Integer id, String name, String dosage) {
        this.id = id;
        this.name = name;
        this.dosage = dosage;
    }

    public Medication(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public Set<SideEffect> getSideEffectSet() {
        return sideEffectSet;
    }

    public void setSideEffectSet(Set<SideEffect> sideEffectSet) {
        this.sideEffectSet = sideEffectSet;
    }
}
