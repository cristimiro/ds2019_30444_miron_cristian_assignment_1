package com.ds.Assignment1.entities;


import javax.persistence.*;

import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medicationPlan")
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @ManyToMany
    @JoinTable(
            name = "medication_plan_medication",
            joinColumns = @JoinColumn(name = "medicationPlan_id"),
            inverseJoinColumns = @JoinColumn(name = "medication_id"))
    private Set<Medication> medicationSet;

    @ManyToMany(mappedBy = "medicationPlanSet", fetch = FetchType.LAZY)
    private Set<Patient> patientSet;

    @Column(name="period")
    private Integer period;

    public MedicationPlan(Integer id, Integer period) {
        this.id = id;
        this.period = period;
    }

    public MedicationPlan(Integer id, Set<Medication> medicationSet, Integer period) {
        this.id = id;
        this.medicationSet = medicationSet;
        this.period = period;
    }


    public MedicationPlan(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Medication> getMedicationSet() {
        return medicationSet;
    }

    public void setMedicationSet(Set<Medication> medicationSet) {
        this.medicationSet = medicationSet;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public void setPatientSet(Set<Patient> patientSet) {
        this.patientSet = patientSet;
    }
}
