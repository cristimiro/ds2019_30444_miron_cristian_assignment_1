package com.ds.Assignment1.entities;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Entity
public class Caregiver extends User{

    @ManyToMany
    @JoinTable(
            name = "caregiver_patient",
            joinColumns = @JoinColumn(name = "caregiver_id"),
            inverseJoinColumns = @JoinColumn(name = "patient_id"))
    private Set<Patient> patientSet;

    public Caregiver(Integer id,String username, String password, String name, Date birth_date, String gender, String address) {
        super(id,username, password, name, birth_date, gender, address);
    }

    public Caregiver(Integer id, String username, String name) {
        super(id,username,name);
    }

    public Caregiver(){

    }

    public void addPatient(Patient patient){
        this.patientSet.add(patient);
    }

    public void removePatient(Patient patient){
        this.patientSet.remove(patient);
    }


    public Set<Patient> getPatientSet() {
        return patientSet;
    }

    public void setPatientSet(Set<Patient> patientSet) {
        this.patientSet = patientSet;
    }
}
