package com.ds.Assignment1.entities;
import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Entity
public class Patient extends User {

//    private File medicalRecord;

    @ManyToMany(mappedBy = "patientSet", fetch = FetchType.EAGER)
    private Set<Caregiver> caregiverSet;

    @ManyToMany
    @JoinTable(
            name = "medication_plan_patient",
            joinColumns = @JoinColumn(name = "patient_id"),
            inverseJoinColumns = @JoinColumn(name = "medication_plan_id"))
    private Set<MedicationPlan> medicationPlanSet;


    public Patient(Integer id, String username, String password, String name, Date birth_date, String gender, String address) {
        super(id, username, password, name, birth_date, gender, address);
    }

    public Patient(Integer id,String username, String name) {
        super(id, username,name);
    }

    public Patient(){

    }

    public Set<MedicationPlan> getMedicationPlanSet() {
        return medicationPlanSet;
    }

    public void setMedicationPlanSet(Set<MedicationPlan> medicationPlanSet) {
        this.medicationPlanSet = medicationPlanSet;
    }

    public void setCaregiverSet(Set<Caregiver> caregiverSet) {
        this.caregiverSet = caregiverSet;
    }
}
