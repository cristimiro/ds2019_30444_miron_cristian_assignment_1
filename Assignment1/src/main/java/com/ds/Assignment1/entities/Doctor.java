package com.ds.Assignment1.entities;

import lombok.Data;

import javax.persistence.Entity;
import java.sql.Date;

@Data
@Entity
public class Doctor extends User {

    public Doctor(Integer id, String username, String password, String name, Date birth_date, String gender, String address) {
        super(id, username, password, name, birth_date, gender, address);
    }
    public Doctor(Integer id,String username, String name) {
        super(id, username,name);
    }

    public Doctor(){

    }
}
