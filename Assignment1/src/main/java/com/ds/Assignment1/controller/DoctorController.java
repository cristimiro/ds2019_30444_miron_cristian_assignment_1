package com.ds.Assignment1.controller;

import com.ds.Assignment1.dto.DoctorDTO;
import com.ds.Assignment1.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/doctors")
public class DoctorController {

    private final DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService){
        this.doctorService = doctorService;
    }

    @GetMapping(value = "/{id}")
    public DoctorDTO findById(@PathVariable("id") Integer id) {
        return doctorService.findUserById(id);
    }

    @GetMapping()
    public List<DoctorDTO> findAll() {
        return doctorService.findAll();
    }

    @PostMapping()
    public Integer insertUserDTO(@RequestBody DoctorDTO doctorDTO) {
        return doctorService.insert(doctorDTO);
    }

    @PutMapping()
    public Integer updateUser(@RequestBody DoctorDTO doctorDTO) {
        return doctorService.update(doctorDTO);
    }

    @DeleteMapping(value="/{id}")
    public void delete(@PathVariable Integer id) {
        doctorService.delete(id);
    }

}
