package com.ds.Assignment1.controller;

import com.ds.Assignment1.dto.SideEffectDTO;
import com.ds.Assignment1.dto.SideEffectViewDTO;
import com.ds.Assignment1.services.SideEffectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/sideEffect")
public class SideEffectController {

    private final SideEffectService sideEffectService;

    @Autowired
    public SideEffectController(SideEffectService sideEffectService){
        this.sideEffectService = sideEffectService;
    }

    @GetMapping(value = "/{id}")
    public SideEffectViewDTO findById(@PathVariable("id") Integer id) {
        return sideEffectService.findUserById(id);
    }

    @GetMapping()
    public List<SideEffectViewDTO> findAll() {
        return sideEffectService.findAll();
    }

    @PostMapping()
    public Integer insertUserDTO(@RequestBody SideEffectDTO sideEffectDTO) {
        return sideEffectService.insert(sideEffectDTO);
    }

    @PutMapping()
    public Integer updateUser(@RequestBody SideEffectDTO sideEffectDTO) {
        return sideEffectService.update(sideEffectDTO);
    }

    @DeleteMapping()
    public void delete(@RequestBody SideEffectViewDTO sideEffectViewDTO) {
        sideEffectService.delete(sideEffectViewDTO);
    }


}
