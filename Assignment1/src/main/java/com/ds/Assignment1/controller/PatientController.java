package com.ds.Assignment1.controller;

import com.ds.Assignment1.dto.PatientDTO;
import com.ds.Assignment1.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/patients")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService){
        this.patientService = patientService;
    }

    @GetMapping(value = "/{id}")
    public PatientDTO findById(@PathVariable("id") Integer id) {
        return patientService.findUserById(id);
    }

    @GetMapping
    public List<PatientDTO> findAll() {
        return patientService.findAll();
    }

    @PostMapping(value = "/add-plan")
    public void mapPatientPlan(@RequestBody Map<String, String> data) {
        patientService.mapMedPlan(data);
    }

    @PostMapping
    public Integer insertUserDTO(@RequestBody PatientDTO patientDTO) {
        return patientService.insert(patientDTO);
    }

    @PutMapping
    public Integer updateUser(@RequestBody PatientDTO patientDTO) {
        return patientService.update(patientDTO);
    }

    @DeleteMapping(value="/{id}")
    public void delete(@PathVariable Integer id) {
        patientService.delete(id);
    }

}
