package com.ds.Assignment1.controller;

import com.ds.Assignment1.dto.MedicationDTO;
import com.ds.Assignment1.dto.MedicationViewDTO;
import com.ds.Assignment1.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService){
        this.medicationService = medicationService;
    }

    @GetMapping(value = "/{id}")
    public MedicationDTO findById(@PathVariable("id") Integer id) {
        return medicationService.findUserById(id);
    }

    @GetMapping()
    public List<MedicationDTO> findAll() {
        return medicationService.findAll();
    }

    @PostMapping()
    public Integer insertUserDTO(@RequestBody MedicationDTO medicationDTO) {
        return medicationService.insert(medicationDTO);
    }

    @PutMapping()
    public Integer updateUser(@RequestBody MedicationDTO medicationDTO) {
        return medicationService.update(medicationDTO);
    }

    @DeleteMapping(value="/{id}")
    public void delete(@PathVariable Integer id) {
        medicationService.delete(id);
    }
    
}
