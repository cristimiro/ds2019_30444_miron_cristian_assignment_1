package com.ds.Assignment1.controller;

import com.ds.Assignment1.dto.MedicationPlanDTO;
import com.ds.Assignment1.services.MedicationPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/medication-plan")
public class MedicationPlanController {

    private final MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService){
        this.medicationPlanService = medicationPlanService;
    }

    @GetMapping(value = "/{id}")
    public MedicationPlanDTO findById(@PathVariable("id") Integer id) {
        return medicationPlanService.findUserById(id);
    }

    @GetMapping()
    public List<MedicationPlanDTO> findAll() {
        return medicationPlanService.findAll();
    }

    @PostMapping()
    public Integer insertUserDTO(@RequestBody MedicationPlanDTO medicationDTO) {
        return medicationPlanService.insert(medicationDTO);
    }

    @PostMapping(value = "/add")
    public void mapPatientCaregiver(@RequestBody Map<String, String> data) {
        medicationPlanService.mapMedicine(data);
    }

    @PutMapping()
    public Integer updateUser(@RequestBody MedicationPlanDTO medicationDTO) {
        return medicationPlanService.update(medicationDTO);
    }

    @DeleteMapping(value="/{id}")
    public void delete(@PathVariable Integer id) {
        medicationPlanService.delete(id);
    }
}
