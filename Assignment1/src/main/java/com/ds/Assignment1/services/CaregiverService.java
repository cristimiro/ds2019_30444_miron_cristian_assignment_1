package com.ds.Assignment1.services;

import com.ds.Assignment1.dto.CaregiverDTO;
import com.ds.Assignment1.dto.builders.CaregiverBuilder;
import com.ds.Assignment1.entities.Caregiver;
import com.ds.Assignment1.entities.Patient;
import com.ds.Assignment1.repositories.CaregiverRepository;
import com.ds.Assignment1.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CaregiverService {
    private final CaregiverRepository caregiverRepository;
    private final PatientRepository patientRepository;
    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository, PatientRepository patientRepository) {
        this.caregiverRepository = caregiverRepository;
        this.patientRepository = patientRepository;
    }

    public CaregiverDTO findUserById(Integer id) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(id);
        System.out.println(caregiver.get().getPatientSet().size());
        return CaregiverBuilder.generateDTOFromEntity(caregiver.get());
    }

    public List<CaregiverDTO> findAll() {
        List<Caregiver> caregivers = caregiverRepository.getAllOrdered();
        return caregivers.stream()
                .map(CaregiverBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(CaregiverDTO caregiverDTO) {
        return caregiverRepository
                .save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO))
                .getId();
    }

    public Integer update(CaregiverDTO caregiverDTO) {
//        Optional<Caregiver> optionalCaregiver = caregiverRepository.findById(caregiverDTO.getId());
//        Caregiver caregiver = optionalCaregiver.get();
//        caregiverDTO.setAddress(caregiver.getAddress());
//        caregiverDTO.setBirth_date(caregiver.getBirth_date());
//        caregiverDTO.setGender(caregiver.getGender());
//        caregiverDTO.setPassword(caregiver.getPassword());
        return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO)).getId();
    }

    public void update(Caregiver caregiver) {
        caregiverRepository.save(caregiver);
    }

    public void delete(Integer id){
        this.caregiverRepository.deleteById(id);
    }

    public void getPatients(Integer id){

    }

    public void mapPatient(Map<String, String> data){
        Integer cid = 0;
        Integer pid = 0;
        for(Map.Entry e : data.entrySet()){
            if (e.getKey().equals("caregiver")){
                cid = Integer.parseInt((String) e.getValue());
            }
            if (e.getKey().equals("patient")){
                pid = Integer.parseInt((String) e.getValue());
            }

        }
        System.out.println("care: " + pid + " " + cid);
        Optional<Caregiver> caregiverOptional = caregiverRepository.findById(cid);
        Caregiver caregiver = caregiverOptional.get();

        Optional<Patient> patientOptional = patientRepository.findById(pid);
        Patient patient = patientOptional.get();
        Set<Patient> patients = new HashSet<>();
        patients = caregiver.getPatientSet();
        patients.add(patient);
        caregiver.setPatientSet(patients);
        this.update(caregiver);
//        Caregiver caregiver = caregiverRepository.findById(stringList.)
    }
}
