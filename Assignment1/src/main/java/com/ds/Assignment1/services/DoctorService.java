package com.ds.Assignment1.services;

import com.ds.Assignment1.dto.DoctorDTO;
import com.ds.Assignment1.dto.DoctorViewDTO;
import com.ds.Assignment1.dto.builders.DoctorBuilder;
import com.ds.Assignment1.dto.builders.DoctorViewBuilder;
import com.ds.Assignment1.entities.Doctor;
import com.ds.Assignment1.repositories.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DoctorService {

    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    public DoctorDTO findUserById(Integer id) {
        Optional<Doctor> doctor = doctorRepository.findById(id);
        return DoctorBuilder.generateDTOFromEntity(doctor.get());
    }

    public List<DoctorDTO> findAll() {
        List<Doctor> doctors = doctorRepository.getAllOrdered();
        return doctors.stream()
                .map(DoctorBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(DoctorDTO doctorDTO) {
        return doctorRepository
                .save(DoctorBuilder.generateEntityFromDTO(doctorDTO))
                .getId();
    }

    public Integer update(DoctorDTO doctorDTO) {
        return doctorRepository.save(DoctorBuilder.generateEntityFromDTO(doctorDTO)).getId();
    }

    public void delete(Integer id) {
        this.doctorRepository.deleteById(id);
    }
}
