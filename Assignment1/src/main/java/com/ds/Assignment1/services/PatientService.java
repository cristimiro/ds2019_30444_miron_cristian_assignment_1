package com.ds.Assignment1.services;

import com.ds.Assignment1.dto.PatientDTO;
import com.ds.Assignment1.dto.builders.PatientBuilder;
import com.ds.Assignment1.entities.MedicationPlan;
import com.ds.Assignment1.entities.Patient;
import com.ds.Assignment1.repositories.MedicationPlanRepository;
import com.ds.Assignment1.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private final PatientRepository patientRepository;
    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository, MedicationPlanRepository medicationPlanRepository) {
        this.patientRepository = patientRepository;
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public PatientDTO findUserById(Integer id) {
        Optional<Patient> patient = patientRepository.findById(id);
        return PatientBuilder.generateDTOFromEntity(patient.get());
    }

    public List<PatientDTO> findAll() {
        List<Patient> patients = patientRepository.findAll();
        System.out.println(patients.size());
        return patients.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(PatientDTO patientDTO) {
        boolean a = true;
        return patientRepository
                .save(PatientBuilder.generateEntityFromDTO(patientDTO))
                .getId();
    }

    public Integer update(PatientDTO patientDTO) {
        return patientRepository.save(PatientBuilder.generateEntityFromDTO(patientDTO)).getId();
    }

    public void update(Patient patient) {
       patientRepository.save(patient);
    }

    public void delete(Integer id){
        this.patientRepository.deleteById(id);
    }

    public void mapMedPlan(Map<String, String> data){
        Integer medId = 0;
        Integer pid = 0;
        for(Map.Entry e : data.entrySet()){
            if (e.getKey().equals("medicationPlan")){
                medId = Integer.parseInt((String) e.getValue());
            }
            if (e.getKey().equals("patient")){
                pid = Integer.parseInt((String) e.getValue());
            }

        }
        Optional<Patient> patientOptional = patientRepository.findById(pid);
        Patient patient = patientOptional.get();

        Optional<MedicationPlan> medicationPlanOptional = medicationPlanRepository.findById(medId);
        MedicationPlan medicationPlan = medicationPlanOptional.get();
        Set<MedicationPlan> medicationPlans = new HashSet<>();
        medicationPlans = patient.getMedicationPlanSet();
        medicationPlans.add(medicationPlan);
        patient.setMedicationPlanSet(medicationPlans);
        this.update(patient);
    }
}
