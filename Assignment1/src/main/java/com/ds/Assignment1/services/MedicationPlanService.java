package com.ds.Assignment1.services;

import com.ds.Assignment1.dto.MedicationPlanDTO;
import com.ds.Assignment1.dto.builders.MedicationPlanBuilder;
import com.ds.Assignment1.entities.Medication;
import com.ds.Assignment1.entities.MedicationPlan;
import com.ds.Assignment1.repositories.MedicationPlanRepository;
import com.ds.Assignment1.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository, MedicationRepository medicationRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
        this.medicationRepository = medicationRepository;
    }

    public MedicationPlanDTO findUserById(Integer id) {
        Optional<MedicationPlan> medication = medicationPlanRepository.findById(id);
        return MedicationPlanBuilder.generateDTOFromEntity(medication.get());
    }

    public List<MedicationPlanDTO> findAll() {
        List<MedicationPlan> medications = medicationPlanRepository.findAll();
        return medications.stream()
                .map(MedicationPlanBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationPlanDTO medicationPlanDTO) {
        return medicationPlanRepository
                .save(MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO))
                .getId();
    }

    public Integer update(MedicationPlanDTO medicationPlanDTO) {
        return medicationPlanRepository.save(MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO)).getId();
    }

    public void insert(MedicationPlan medicationPlan){
        medicationPlanRepository.save(medicationPlan);
    }

    public void mapMedicine(Map<String, String> data){
        Integer mid = 0;
        Integer period = 0;
        for(Map.Entry e : data.entrySet()){
            if (e.getKey().equals("period")){
                period = Integer.parseInt((String) e.getValue());
            }
            if (e.getKey().equals("m_id")){
                mid = Integer.parseInt((String) e.getValue());
            }
        }

        System.out.println("care: " + period + " " + mid);

        MedicationPlan medicationPlan = new MedicationPlan();
        medicationPlan.setPeriod(period);

        Optional<Medication> medicationOptional = medicationRepository.findById(mid);
        Medication medication = medicationOptional.get();
        Set<Medication> mSet = new HashSet<>();
        mSet.add(medication);
        medicationPlan.setMedicationSet(mSet);
        this.insert(medicationPlan);
    }

    public void delete(Integer id) {
        this.medicationPlanRepository.deleteById(id);
    }
}
