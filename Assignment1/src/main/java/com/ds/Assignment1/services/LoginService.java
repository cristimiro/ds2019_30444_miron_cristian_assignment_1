package com.ds.Assignment1.services;

import com.ds.Assignment1.entities.User;
import com.ds.Assignment1.repositories.LoginRepository;
import com.ds.Assignment1.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoginService {

    private final LoginRepository loginRepository;

    @Autowired
    UserRepository userRepository;

    public LoginService(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    public User findUser(String username){
        return userRepository.getUserByUsername(username);
    }
}
