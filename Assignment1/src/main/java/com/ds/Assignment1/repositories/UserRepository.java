package com.ds.Assignment1.repositories;

import com.ds.Assignment1.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Integer>{
    @Query(value = "SELECT u " +
            "FROM User u " +
            "ORDER BY u.id")
    List<User> getAllOrdered();

    User getUserByUsername(String username);


}
