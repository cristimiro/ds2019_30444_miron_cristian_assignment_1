package com.ds.Assignment1.dto.builders;

import com.ds.Assignment1.dto.PatientViewDTO;
import com.ds.Assignment1.entities.Patient;

public class PatientViewBuilder {
    public static PatientViewDTO generateDTOFromEntity(Patient patient){
        return new PatientViewDTO(patient.getId(), patient.getUsername(), patient.getName());
    }

    public static Patient generateEntityFromDTO(PatientViewDTO patientViewDTO){
        return new Patient(patientViewDTO.getId(),patientViewDTO.getUsername(), patientViewDTO.getName());
    }
}
