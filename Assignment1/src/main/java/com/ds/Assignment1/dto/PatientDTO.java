package com.ds.Assignment1.dto;

import com.ds.Assignment1.entities.MedicationPlan;

import java.sql.Date;
import java.util.Objects;
import java.util.Set;

public class PatientDTO {

    private Integer id;
    private String username;
    private String password;
    private String name;
    private Date birth_date;
    private String gender;
    private String address;
    private String role;
    private Set<MedicationPlan> medicationPlanSet;

    public PatientDTO(Integer id, String username, String password, String name, Date birth_date, String gender, String address, Set<MedicationPlan> medicationPlanSet) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.name = name;
        this.birth_date = birth_date;
        this.gender = gender;
        this.address = address;
        this.role = "patient";
        this.medicationPlanSet = medicationPlanSet;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirth_date() {
        return birth_date;
    }

    public void setBirth_date(Date birth_date) {
        this.birth_date = birth_date;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Set<MedicationPlan> getMedicationPlanSet() {
        return medicationPlanSet;
    }

    public void setMedicationPlanSet(Set<MedicationPlan> medicationPlanSet) {
        this.medicationPlanSet = medicationPlanSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDTO that = (PatientDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(username, that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username);
    }
}
