package com.ds.Assignment1.dto.builders;

import com.ds.Assignment1.dto.CaregiverDTO;
import com.ds.Assignment1.entities.Caregiver;

public class CaregiverBuilder {

    public CaregiverBuilder(){

    }

    public static CaregiverDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverDTO(
                caregiver.getId(),
                caregiver.getUsername(),
                caregiver.getPassword(),
                caregiver.getName(),
                caregiver.getBirth_date(),
                caregiver.getGender(),
                caregiver.getAddress(),
                caregiver.getPatientSet()
        );
    }

    public static Caregiver generateEntityFromDTO(CaregiverDTO caregiverDTO){
        return new Caregiver(
                caregiverDTO.getId(),
                caregiverDTO.getUsername(),
                caregiverDTO.getPassword(),
                caregiverDTO.getName(),
                caregiverDTO.getBirth_date(),
                caregiverDTO.getGender(),
                caregiverDTO.getAddress()
        );
    }
}
