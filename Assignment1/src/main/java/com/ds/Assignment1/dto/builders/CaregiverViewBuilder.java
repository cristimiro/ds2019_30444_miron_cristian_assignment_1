package com.ds.Assignment1.dto.builders;

import com.ds.Assignment1.dto.CaregiverViewDTO;
import com.ds.Assignment1.entities.Caregiver;

public class CaregiverViewBuilder {

    public static CaregiverViewDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverViewDTO(caregiver.getId(), caregiver.getUsername(), caregiver.getName());
    }

    public static Caregiver generateEntityFromDTO(CaregiverViewDTO caregiverViewDTO){
        return new Caregiver(caregiverViewDTO.getId(),caregiverViewDTO.getUsername(), caregiverViewDTO.getName());
    }
}
