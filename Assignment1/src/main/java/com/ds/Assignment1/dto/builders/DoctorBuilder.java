package com.ds.Assignment1.dto.builders;

import com.ds.Assignment1.dto.DoctorDTO;
import com.ds.Assignment1.entities.Doctor;

public class DoctorBuilder {

    public DoctorBuilder(){

    }

    public static DoctorDTO generateDTOFromEntity(Doctor doctor){
        return new DoctorDTO(
                doctor.getId(),
                doctor.getUsername(),
                doctor.getPassword(),
                doctor.getName(),
                doctor.getBirth_date(),
                doctor.getGender(),
                doctor.getAddress()
        );
    }

    public static Doctor generateEntityFromDTO(DoctorDTO doctorDTO){
        return new Doctor(
                doctorDTO.getId(),
                doctorDTO.getUsername(),
                doctorDTO.getPassword(),
                doctorDTO.getName(),
                doctorDTO.getBirth_date(),
                doctorDTO.getGender(),
                doctorDTO.getAddress()
        );
    }
}
