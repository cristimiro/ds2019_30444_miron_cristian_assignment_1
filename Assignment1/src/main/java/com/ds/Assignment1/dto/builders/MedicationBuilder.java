package com.ds.Assignment1.dto.builders;

import com.ds.Assignment1.dto.MedicationDTO;
import com.ds.Assignment1.dto.MedicationViewDTO;
import com.ds.Assignment1.entities.Medication;

public class MedicationBuilder {

    public MedicationBuilder(){

    }

    public static MedicationDTO generateDTOFromEntity(Medication medication){
        return new MedicationDTO(
                medication.getId(), medication.getName(), medication.getDosage()
        );
    }

    public static Medication generateEntityFromDTO(MedicationDTO medicationDTO){
        return new Medication(
                medicationDTO.getId(),medicationDTO.getName(), medicationDTO.getDosage()
        );
    }

}
