package com.ds.Assignment1.dto;

public class SideEffectViewDTO {
    private Integer id;
    private String sideEffect;


    public SideEffectViewDTO(Integer id, String sideEffect) {
        this.id = id;
        this.sideEffect = sideEffect;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSideEffect() {
        return sideEffect;
    }

    public void setSideEffect(String sideEffect) {
        this.sideEffect = sideEffect;
    }
}
