package com.ds.Assignment1.dto;

import com.ds.Assignment1.entities.Medication;

import java.util.Set;

public class MedicationPlanDTO {

    private Integer id;
    private Integer period;
    private Set<Medication> medicationSet;

    public MedicationPlanDTO(Integer id,  Integer period,Set<Medication> medicationSet) {
        this.id = id;
        this.period = period;
        this.medicationSet = medicationSet;
    }

    public MedicationPlanDTO(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Set<Medication> getMedicationSet() {
        return medicationSet;
    }

    public void setMedicationSet(Set<Medication> medicationSet) {
        this.medicationSet = medicationSet;
    }
}
