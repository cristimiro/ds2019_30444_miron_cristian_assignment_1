package com.ds.Assignment1.dto.builders;

import com.ds.Assignment1.dto.SideEffectDTO;
import com.ds.Assignment1.dto.SideEffectViewDTO;
import com.ds.Assignment1.entities.SideEffect;

public class SideEffectViewBuilder {

    public static SideEffectViewDTO generateDTOFromEntity(SideEffect sideEffect){
        return new SideEffectViewDTO(sideEffect.getId(),sideEffect.getSideEffect());
    }

    public static SideEffect generateEntityFromDTO (SideEffectViewDTO sideEffectViewDTO){
        return new SideEffect(sideEffectViewDTO.getId(),sideEffectViewDTO.getSideEffect());
    }

}
