package com.ds.Assignment1.dto.builders;

import com.ds.Assignment1.dto.PatientDTO;
import com.ds.Assignment1.entities.Patient;

public class PatientBuilder {

    public PatientBuilder(){

    }

    public static PatientDTO generateDTOFromEntity(Patient patient){
        return new PatientDTO(
                patient.getId(),
                patient.getUsername(),
                patient.getPassword(),
                patient.getName(),
                patient.getBirth_date(),
                patient.getGender(),
                patient.getAddress(),
                patient.getMedicationPlanSet()
        );
    }

    public static Patient generateEntityFromDTO(PatientDTO patientDTO){
        return new Patient(
                patientDTO.getId(),
                patientDTO.getUsername(),
                patientDTO.getPassword(),
                patientDTO.getName(),
                patientDTO.getBirth_date(),
                patientDTO.getGender(),
                patientDTO.getAddress()
        );
    }

}
